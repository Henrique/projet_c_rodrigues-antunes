all: IP.o main

IP.o: IP.c
	gcc -c IP.c

main: main.c
	gcc IP.o main.c -o main