/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 2                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Analyse adresse IP                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Antunes Dos Santos Nabais Daniel                             *
*                                                                             *
*  Nom-prénom2 : Rodrigues Henrique                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : main.c                                                    *
*                                                                             *
******************************************************************************/

#include "IP.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main(){
	char ip[] = "10000000.00001010.11011000.00100111/10101";
	char ipChamp1[8+1];
	char ipChamp2[8+1];
	char ipChamp3[8+1];
	char ipChamp4[8+1];
	char Masque[8+1];
	
	if(!verifFormat(ip)){
		printf("Format de l'Ip Invalide\n");
	}else{
		printf("Format de l'Ip Valide\n");
	}
	
	extraireChamps(ip,ipChamp1,ipChamp2,ipChamp3,ipChamp4,Masque);
	
	int valeurip1 =  convertValNumip( ipChamp1 );
	int valeurip2 =  convertValNumip( ipChamp2 );
	int valeurip3 =  convertValNumip( ipChamp3 );
	int valeurip4 =  convertValNumip( ipChamp4 );
	int valeurmasque =  convertValNummasque( Masque );
	affichage (valeurip1 , valeurip2 , valeurip3 , valeurip4 , valeurmasque );
	
		//Affichage dans un fichier texte
	FILE* fichier;
	fichier = fopen("resultat.txt", "w");
	if(!verifFormat(ip)){
		fprintf(fichier,"Format : Invalide\n");
	}else{
		fprintf(fichier,"Format : Valide\n");
	}
	fprintf(fichier, decodeIP( valeurip1 ,   valeurip2 ,  valeurip3 ,   valeurip4 ));
	fprintf (fichier, "l'ip est : %d.%d.%d.%d/%d\n" , convertValNumip( ipChamp1 ) , convertValNumip( ipChamp2 ) , convertValNumip( ipChamp3 ), convertValNumip( ipChamp4 ) , convertValNummasque( Masque ) );
}