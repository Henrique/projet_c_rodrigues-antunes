/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 2                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Analyse adresse IP                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Antunes Dos Santos Nabais Daniel                             *
*                                                                             *
*  Nom-prénom2 : Rodrigues Henrique                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : IP.h                                                      *
*                                                                             *
******************************************************************************/

typedef enum {false, true} bool;

bool verifFormat(char *ip);

void extraireChamps(char *ip,char *ipChamp1,char *ipChamp2,char *ipChamp3,char *ipChamp4,char *Masque);

int convertValNumip ( char* ipChamp1 );

int convertValNummasque ( char* Masque);

char *decodeIP ( int valeurip1 ,  int valeurip2 ,  int valeurip3 ,  int valeurip4 );

void affichage ( int valeurip1 ,  int  valeurip2 ,  int  valeurip3 ,  int  valeurip4 , int  valeurmasque );
